/**
 * @module Environments
 * @internal
 */
import {argv} from 'yargs';
import { Config as DevelopmentConfig } from './environment.development';
import { Config as ProductionConfig } from './environment.production';

export const ENVIRONMENTS = {
    DEVELOPMENT: 'dev',
    PRODUCTION: 'prod'
};

class Environments {
  get config() {
    if (argv.env !== null) {
      if (argv.env === ENVIRONMENTS.DEVELOPMENT) {
        return DevelopmentConfig;
      } else {
        return ProductionConfig;
      }
    } else {
      return ProductionConfig;
    }   
  }
}

export const Environment = new Environments();