/**
 * Development Config
 * @module Environments.Production
 * @internal
 */
export const Config = {
    "production": true,
    "settings": {
        "port": 80,
        "timeout": 600000
    },
    "geotab": {
    }
};
