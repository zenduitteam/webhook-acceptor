/**
 * Development Config
 * @module Environments.Development
 * @internal
 */
export const Config = {
    "production": false,
    "settings": {
        "port": 8080,
        "timeout": 600000
    },
    "geotab": {
    }
};
