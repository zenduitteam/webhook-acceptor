/**
 * @module LoadAddin
 * @internal
 */
import { ServerController } from './components/server.component';
import { Environment } from './environments/environment';

import * as functions from 'firebase-functions';
import * as express from 'express';
import * as http from 'http';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';

const config = Environment.config;
const app = express();

const PRODUCTION = config.production || false;
const PORT = process.env.PORT || config.settings.port;
const TIMEOUT = config.settings.timeout;

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

if(!PRODUCTION) 
  app.use("/documentation/internal", express.static(__dirname + '/../documentation/internal'));

app.use("/documentation/api", express.static(__dirname + '/../documentation/api'));
app.use('/api', ServerController);
  
app.set('trust proxy', true);

const server = http.createServer(app);

server.timeout = TIMEOUT;  
server.listen(PORT, () => console.log('Server Listening'));

exports.v1 = functions.https.onRequest(app);