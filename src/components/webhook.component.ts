/**
 * @module Webhook
 * @internal
 */
import { Environment } from '../environments/environment';

/**
 * Interface for WebhookParams
 * @interface WebhookParams
 */
export interface WebhookParams {
  Database: string;
  Address: string;
  Date: string;
  Device: string;
  DeviceComments: string;
  DeviceGroups: string;
  DeviceID: string;
  DeviceWithDriverName: string;
  Diagnostic: string;
  DriverComment: string;
  DriverFirstName: string;
  DriverGroups: string;
  DriverLastName: string;
  DriverName: string;
  DVIRDefect: string;
  Latitude: string;
  Longitude: string;
  Odometer: string;
  Rule: string;
  SerialNumber: string;
  Speed: string;
  SpeedLimit: string;
  Time: string;
  Timezone: string;
  Vin: string;
  Zone: string;
  ZoneComment: string;
  ZoneID: string;  
}

/**
 * Interface for WebhookPostOutput
 * @interface WebhookPostOutput
 */
export interface WebhookPostOutput {
  message: string;
}

/**
 * @class HOS
 */
export class Webhook {
  
  constructor() {}
  
  /**
   * Processes Geotab inbound webhook information
   * @param {WebhookParams} params - params sent from geotab
   * @return {Promise<WebhookPostOutput>} Success Message
   */   
  public async post(params:WebhookParams) : Promise<WebhookPostOutput> {
    
    console.log(params);
    
    return {
      "message": "success"
    };
    
  }
  
}
