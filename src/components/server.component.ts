/**
 * @module Server
 * @internal
 */
import { Router, Request, Response } from 'express';

import { Environment } from '../environments/environment';
import { Webhook, WebhookParams, WebhookPostOutput } from './webhook.component';

/**
  * @apiDefine WebhookSuccess
  * @apiSuccess {string} message message if it was a success
  */
  
/**
  * @apiDefine WebhookBodyParam
  * @apiParam (Request Body: JSON) {string} Database Database Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Address Address Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Date Date Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Device Device Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} DeviceComments DeviceComments Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} DeviceGroups DeviceGroups Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} DeviceID DeviceID Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} DeviceWithDriverName DeviceWithDriverName Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Diagnostic Diagnostic Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} DriverComment DriverComment Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} DriverFirstName DriverFirstName Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} DriverGroups DriverGroups Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} DriverLastName DriverLastName Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} DriverName DriverName Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} DVIRDefect DVIRDefect Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Latitude Latitude Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Longitude Longitude Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Odometer Odometer Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Rule Rule Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} SerialNumber SerialNumber Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Speed Speed Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} SpeedLimit SpeedLimit Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Time Time Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Timezone Timezone Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Vin Vin Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} Zone Zone Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} ZoneComment ZoneComment Reported from Geotab Rules engine notification template
  * @apiParam (Request Body: JSON) {string} ZoneID ZoneID Reported from Geotab Rules engine notification template
  */
  

/**
  * @apiDefine WebhookQueryParam
  * @apiParam (Request Query String) {string} Database Database Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Address Address Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Date Date Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Device Device Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} DeviceComments DeviceComments Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} DeviceGroups DeviceGroups Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} DeviceID DeviceID Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} DeviceWithDriverName DeviceWithDriverName Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Diagnostic Diagnostic Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} DriverComment DriverComment Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} DriverFirstName DriverFirstName Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} DriverGroups DriverGroups Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} DriverLastName DriverLastName Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} DriverName DriverName Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} DVIRDefect DVIRDefect Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Latitude Latitude Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Longitude Longitude Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Odometer Odometer Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Rule Rule Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} SerialNumber SerialNumber Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Speed Speed Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} SpeedLimit SpeedLimit Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Time Time Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Timezone Timezone Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Vin Vin Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} Zone Zone Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} ZoneComment ZoneComment Reported from Geotab Rules engine notification template
  * @apiParam (Request Query String) {string} ZoneID ZoneID Reported from Geotab Rules engine notification template
  */

const router: Router = Router();
const WH: Webhook = new Webhook();

const returnResults = async (request, response, call) => {
  
  try {
    
    const results : any = await call;
    
    response.setHeader('Content-Type', 'application/json');
    response.statusCode = 200;
    
    return response.send(results);
    
  } catch(error) {
    
    if(error.stack) console.error(error.stack);
    
    const errorDetails : any = error.error || error; 
    
    response.setHeader('Content-Type', 'application/json');
    response.statusCode = error.statusCode ? error.statusCode : 500;
    
    return response.send(errorDetails); 
    
  } 
  
}

router.get('/', (request: Request, response: Response) => {
  
  const call : any = {
    "message": "Geotab Rule Webhook Tester By Zenduit"
  };
  
  return returnResults(request, response, call);
  
});

/**
 * @api {POST} /webhook Geotab Inbound Webhook POST Method
 * @apiVersion 1.0.0
 * @apiDescription Returns Success Status
 * @apiName Geotab Inbound POST Webhook
 * @apiGroup Geotab
 * @apiUse WebhookBodyParam
 * @apiUse WebhookSuccess
 */
router.post('/webhook', (request: Request, response: Response) => {
  
  const params : WebhookParams = request.body;
  
  const call : Promise<WebhookPostOutput> = WH.post(params);
  
  return returnResults(request, response, call);
  
});

/**
 * @api {GET} /webhook Geotab Inbound Webhook GET Method
 * @apiVersion 1.0.0
 * @apiDescription Returns Success Status
 * @apiName Geotab Inbound GET Webhook
 * @apiGroup Geotab
 * @apiUse WebhookQueryParam
 * @apiUse WebhookSuccess
 */
router.get('/webhook', (request: Request, response: Response) => {
  
  const params : WebhookParams = request.query;
  
  const call : Promise<WebhookPostOutput> = WH.post(params);
  
  return returnResults(request, response, call);
  
});

export const ServerController: Router = router;