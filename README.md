# Geotab Webhook Test Application

Geotab Webhook Test Application, This project was created to show how to accept a geotab webhook call.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
node v9.5.0
npm v5.6.0
├── nodemon@1.15.1
└── typescript@2.7.2
```

### Installing

Install Node and NPM

```
https://nodejs.org/en/download/current/
```


Install NPM Globals

```
npm install -g nodemon
npm install -g typescript
```

Install node packages

```
npm install
```

## Geotab Webhook Setup

GET Method

```
HTTP request type: GET
URL: http(s)://{{localhost:port}}/webhook?Database={Database}&Address={Address}&Date={Date}&Device={Device}&DeviceComments={Device Comments}&DeviceGroups={Device Groups}&DeviceID={Device ID}&DeviceWithDriverName={Device With Driver Name}&Diagnostic={Diagnostic}&DriverComment={Driver Comment}&DriverFirstName={Driver First Name}&DriverGroups={Driver Groups}&DriverLastName={Driver Last Name}&DriverName={Driver Name}&DVIRDefect={DVIR defect}&Latitude={Latitude}&Longitude={Longitude}&Odometer={Odometer}&Rule={Rule}&SerialNumber={Serial number}&Speed={Speed}&SpeedLimit={Speed limit}&Time={Time}&Timezone={Timezone}&Vin={Vin}&Zone={Zone}&ZoneComment={Zone Comment}&ZoneID={Zone ID}
```

POST Method

```
HTTP request type: POST
URL: http(s)://{{localhost:port}}/webhook
POST Data:
{"Database":"{Database}","Address":"{Address}","Date":"{Date}","Device":"{Device}","DeviceComments":"{Device Comments}","DeviceGroups":"{Device Groups}","DeviceID":"{Device ID}","DeviceWithDriverName":"{Device With Driver Name}","Diagnostic":"{Diagnostic}","DriverComment":"{Driver Comment}","DriverFirstName":"{Driver First Name}","DriverGroups":"{Driver Groups}","DriverLastName":"{Driver Last Name}","DriverName":"{Driver Name}","DVIRDefect":"{DVIR defect}","Latitude":"{Latitude}","Longitude":"{Longitude}","Odometer":"{Odometer}","Rule":"{Rule}","SerialNumber":"{Serial number}","Speed":"{Speed}","SpeedLimit":"{Speed limit}","Time":"{Time}","Timezone":"{Timezone}","Vin":"{Vin}","Zone":"{Zone}","ZoneComment":"{Zone Comment}","ZoneID":"{Zone ID}"}
```

## Development

To run application in development environment

```
npm run start-dev
```

## Deployment

This project has not been configured to deploy to a production environment and is used as a learning example of a potential implementation of a call that excepts and processes geotab inbound webhooks.

## Additional Documentation

You can visit the following address for information on how to interact with the API calls

```
http(s)://{{localhost:port}}/documentation/api/
```

If server is run in developement mode you can visit the following address for documentation on internal functions

```
http(s)://{{localhost:port}}/documentation/internal/
```


## Built With

* [TypeScript](https://www.typescriptlang.org/docs/home.html) - Typed superset of JavaScript that compiles to plain Javascript

## Authors

* **Shane Graham** - *Initial Work* - [shane_zenduit](https://bitbucket.org/shane_zenduit/)
